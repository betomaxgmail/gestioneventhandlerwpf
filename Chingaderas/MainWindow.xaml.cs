﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chingaderas
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Btn1_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Hola mundo");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //btn1.Click -= Btn1_Click;
            //btn1.Click += Btn1_Click;            
            //btn1.PreviewMouseUp += OtraCosa;
        }

        private void OtraCosa(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Otro mensaje");
        }
    }
}
